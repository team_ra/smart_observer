#include <SoftwareSerial.h>

#define POT_PIN A0

SoftwareSerial s(5,6);

int i;       //for max distance A0 0-1023
float mv;
float tempC;
 
void setup() {
  s.begin(9600);
}
 
void loop() {
  i=analogRead(POT_PIN);
  mv = ( i/1024.0)*5000;
  tempC = mv/10;
  if(s.available()>0){
    char c = s.read();
    if(c=='t'){
      s.write(tempC);
    }
  }
}
