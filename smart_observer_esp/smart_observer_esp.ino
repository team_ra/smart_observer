#include <ESP8266WiFi.h>    //For ESP8266
#include <SoftwareSerial.h> //For ESP8266 and Arduino communication

#define SERVER_PORT 8000 //Port 8000 NodeMCU LabVIEW
#define CALIBRATION_TIME_SEC 10 //PIR calibration time
#define TX 14 //GPIO14 = D5
#define RX 12 //GPIO12 = D6
#define LED_RED D8//GPIO10 = SD3
#define LED_GREEN D0 //GPIO09 = SD2
#define LED_BLUE 13 //GPIO13 = D7
#define ECHO_PIN 4 //GPIO4 = D2
#define TRIG_PIN 2 //GPIO2 = D4
#define PIR_PIN 5 //GPIO5 = D1
#define POT_PIN A0 

const char* ssid = "TelefonoMi";      //WiFi NodeMCU Login 
const char* password = "cecilia98"; //Password WiFi NodeMCU Login
//const char* ssid = "FASTWEB-D390F1";      //WiFi NodeMCU Login 
//const char* password = "4KCKZYKRGN"; //Password WiFi NodeMCU Login

WiFiServer server(SERVER_PORT);     //object server port 8000
SoftwareSerial s(RX,TX); // (Rx, Tx)

int i;                 //for max distance A0 0-1023
float maxDistance;     //Max distance 10cm-50cm
float tempC;           //Temperature °C

long duration; //scan duration -> sonar
int distance;  //distance -> sonar

void setup() {
  pinMode(TRIG_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT); 
  pinMode(LED_RED, OUTPUT);
  pinMode(LED_GREEN, OUTPUT);
  pinMode(LED_BLUE, OUTPUT);
  pinMode(PIR_PIN, INPUT);
  pinMode(POT_PIN, INPUT);
  Serial.begin(9600);   //Serial Communication
  s.begin(9600);
  WiFi.begin(ssid, password); //Login WiFi password
  Serial.println("\n Connecting...");

  while (WiFi.status() != WL_CONNECTED) {  //Login Serial Monitor -> Login
    delay(500);
    Serial.print(".");
  }
  Serial.println("\n Successfully connected: ");
  Serial.print(WiFi.localIP());
  server.begin();             //TCP NodeMCU Server LabVIEW Client
  Serial.println("\n NodeMCU as a Server Role Started");

  Serial.print("\n Calibrating sensor...");
  for(int i = 0; i < CALIBRATION_TIME_SEC; i++){
    Serial.print(".");
    delay(1000);
  }
  Serial.print("Pir sensor ready");
}

void loop() {  //LED 16
  WiFiClient client = server.available();  //LabVIEW Client NodeMCU
  if (client) {                             //LabVIEW Client if 
    Serial.println("\n Hi...New Client");   //Client Serial Communication
    s.write("p");
    delay(200);
    if (s.available()>0){
      maxDistance=s.read();
    }
    while(1) {   
      int detected = digitalRead(PIR_PIN);
      delay(1000);
      if(detected == HIGH){   
        while(client.available()) {  //LabVIEW Loop
            Serial.println("\n Object detected!");
            digitalWrite(LED_BLUE, HIGH); //led blue LOW
            uint8_t data = client.read();  
            s.write("t");
            delay(200);
            if (s.available()>0){
              tempC = s.read();
            }
            switch (data) {                //data
              case 'a':     //data='a' LabVIEW case, pot
                i=analogRead(POT_PIN);
                maxDistance=(((i*4)/1023)+1)*10;
                Serial.print("\n Max Distance (cm): "); 
                Serial.println(maxDistance);
                client.println(maxDistance); 
                delay(1000);
              break; 
              case 'b':     //data='b' LabVIEW case, temp
                Serial.print("\n Temperature (°C): "); 
                Serial.println(tempC);
                client.println(tempC); 
                delay(1000);
              break; 
              case 'c':                   //data='c' LabVIEW case
                digitalWrite(TRIG_PIN, LOW); //Reset trigPin
                delayMicroseconds(2);
                
                digitalWrite(TRIG_PIN, HIGH); //trigPin HIGH
                delayMicroseconds(10);
                digitalWrite(TRIG_PIN, LOW); //trigPin LOW
                
                duration = pulseIn(ECHO_PIN, HIGH); //echoPin pulseIn ms

                float vs = (331.45 + 0.62*tempC)/10000;
                distance = duration*vs/2; //distance cm
    
                if(distance>maxDistance){
                   distance=0;
                   digitalWrite(LED_RED, HIGH); //led red HIGH
                   digitalWrite(LED_GREEN, LOW); //led green LOW
                }else{
                  digitalWrite(LED_RED, LOW); //led red LOW
                  digitalWrite(LED_GREEN, HIGH); //led green HIGH
                }
                
                Serial.print("\n Distance (cm): "); 
                Serial.println(distance);
                client.println(distance);    
                delay(1000); 
              break;               
            }         
        }
      }else{
        digitalWrite(LED_GREEN, LOW); //led green LOW
        digitalWrite(LED_BLUE, LOW); //led blue LOW
        digitalWrite(LED_RED, LOW); //led red LOW
      }
      if(server.hasClient()) { //LabVIEW Client  
        return;                //Client
      }
    }
  } 
}
